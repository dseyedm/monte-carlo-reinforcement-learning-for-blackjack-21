//
// monte-carlo reinforcement learning for 21/blackjack
//
// exploration-exploitation tradeoff using epsilon:
// epsilon -> 0, exploration only
// epsilon -> 1, exploitation only
// epsilon -> 0.5, exploration as likely as exploitation
//
// note: pcg is used because the standard rand() function has acted strangely in my experience.
//
// sorajsm
//

#include <bits/stdc++.h>
#include <stdint.h>
#define EPS 0.00001
#define SIZE(a) (sizeof(a)/sizeof(*a))
#define check(c) assert(c)
using namespace std;

//
// *Really* minimal PCG32 code / (c) 2014 M.E. O'Neill / pcg-random.org
// Licensed under Apache License 2.0 (NO WARRANTY, etc. see website)
//
typedef struct { uint64_t state;  uint64_t inc; } pcg32_random_t;
uint32_t pcg32_random_r(pcg32_random_t* rng) {
	uint64_t oldstate = rng->state;
	rng->state = oldstate * 6364136223846793005ULL + (rng->inc|1);
	uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
	uint32_t rot = oldstate >> 59u;
	return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}
pcg32_random_t pcg;

struct deck_t {
	int value_remaining[10];
	int total_remaining;
	deck_t() {
		//
		// the # of cards holding the values 1-9 is 4, respectively.
		// the # of cards holding the value 10 is 4*(10, J, Q, K)
		// and of course, the total_remaining is 52. (4*9 + 4*4)
		//
		for(int i = 0; i < 9; ++i) value_remaining[i] = 4;
		value_remaining[9] = 4*4;
		total_remaining = 52;
	}
	bool draw(int& out_card_value) {
		out_card_value = 0;
		if(total_remaining <= 0) return false;
		int val = pcg32_random_r(&pcg)%total_remaining;
		int i;
		for(i = 0; i < (int)SIZE(value_remaining); ++i) {
			val -= value_remaining[i];
			if(val <= 0) break;
		}
		check(i < (int)SIZE(value_remaining));
		--value_remaining[i];
		--total_remaining;
		out_card_value = i + 1;
		return true;
	}
};

struct hand_t {
	int sum;
	bool has_ace;
	inline hand_t() {
		sum = 0;
		has_ace = false;
	}
	inline void receive(int card_value) {
		sum += card_value;
		has_ace |= card_value == 1;
	}
	inline int value() const {
		if(has_ace && sum + 10 <= 21) return sum + 10;
		return sum;
	}
};

//
// |-------------------------state------------------------||--action--|
// [player.sum][player.has_ace][dealer.sum][dealer.has_ace][action_hit]
// 20*2*10*2*2 = 1600 floats.
// 20*2*10*2*2 = 1600 integers.
// 1600*4 bytes = 6.4 kb each
//
float state_action      [20][2][10][2][2];
int   state_action_count[20][2][10][2][2];
struct path_t {
	int player_sum;
	int dealer_sum;
	bool player_has_ace;
	bool dealer_has_ace;
	bool hit;
};
int main() {
	pcg.state = time(NULL);
	for(int i = 0; i < 20; ++i) {
		for(int j = 0; j < 2; ++j) {
			for(int k = 0; k < 10; ++k) {
				for(int l = 0; l < 2; ++l) {
					for(int m = 0; m < 2; ++m) {
						state_action[i][j][k][l][m] = 0.00f;
						state_action_count[i][j][k][l][m] = 0;
					}
				}
			}
		}
	}
	int wins_per_epoch = 0, losses_per_epoch = 0, ties_per_epoch = 0;
	int epoch_update = 50000;
	printf("epoch,win%%,lose%%,tie%%\n");
	const int epoch_stop = 5000000;
	for(int epoch_i = 0; epoch_i < epoch_stop; ++epoch_i) {
		deck_t deck;
		hand_t dealer;
		hand_t player;

		//
		// deal initial cards. one to the dealer; two to the player.
		//
		int cardv;
		check(deck.draw(cardv)); dealer.receive(cardv);
		check(deck.draw(cardv)); player.receive(cardv);
		check(deck.draw(cardv)); player.receive(cardv);
		int dealer_hidden_value;
		check(deck.draw(dealer_hidden_value)); // dealer.receive(dealer_hidden_value);

		//
		// ask the ai whether to hit or stay.
		// record the actions the ai took during this episode in path.
		//
		vector<path_t> path;
		while(player.value() < 21) {
			//
			// in order to show learning, linearly interpolate between explore and exploit until X% of epoch_stop, then exploit forever.
			//
			const int epsilon_1000 = (float)epoch_i/(epoch_stop*0.8f)*1000.0f; // = 0.9 means 90% exploitation
			bool hit = false;
			float* state_action_hit = &state_action[player.value()][player.has_ace][dealer.value()][dealer.has_ace][0];

			int epsilon_rand = epsilon_1000 < 1000 ? pcg32_random_r(&pcg)%1000 : -1;
			if(epsilon_rand < 0 || epsilon_rand < epsilon_1000) {
				//
				// exploit the best known action.
				//
				hit = state_action_hit[0] > state_action_hit[1] ? 0 : 1;
			} else {
				//
				// explore a random action.
				//
				hit = pcg32_random_r(&pcg)%2;
			}

			path_t p;
			p.player_sum = player.value();
			p.dealer_sum = dealer.value();
			p.player_has_ace = player.has_ace;
			p.dealer_has_ace = dealer.has_ace;
			p.hit = hit;
			path.push_back(p);

			if(!hit) break;
			check(deck.draw(cardv)); player.receive(cardv);
		}

		//
		// house plays.
		//
		if(player.value() <= 21) {
			dealer.receive(dealer_hidden_value);
			while(dealer.value() < 17) {
				check(deck.draw(cardv)); dealer.receive(cardv);
			}
		}

		float end_state_reward; {
			if(player.value() > 21 || (dealer.value() <= 21 && dealer.value() > player.value())) {
				end_state_reward = -1.00f;
				++losses_per_epoch;
			} else if(dealer.value() > 21 || (dealer.value() <= 21 && dealer.value() < player.value())) {
				end_state_reward = 1.00f;
				++wins_per_epoch;
			} else {
				end_state_reward = 0.00f;
				++ties_per_epoch;
			}
		}

		//
		// update the path taken to using this end_state_reward.
		//
		for(size_t p_i = 0; p_i < path.size(); ++p_i) {
			const float this_reward = end_state_reward;
			const path_t& p = path[p_i];
			int& this_stac_count = state_action_count[p.player_sum][p.player_has_ace][p.dealer_sum][p.dealer_has_ace][p.hit];
			float& this_stac = state_action[p.player_sum][p.player_has_ace][p.dealer_sum][p.dealer_has_ace][p.hit];
			++this_stac_count;
			this_stac += (1.0f/this_stac_count)*(this_reward-this_stac);
		}

		//
		// print any epoch information.
		//
		if(epoch_update > 0 && (epoch_i + 1)%epoch_update == 0) {
			printf(
				"%d,%3.2f,%3.2f,%3.2f\n",
				epoch_i + 1,
				(float)wins_per_epoch/epoch_update*100.0f,
				(float)losses_per_epoch/epoch_update*100.0f,
				(float)ties_per_epoch/epoch_update*100.0f
			);
			wins_per_epoch = 0;
			losses_per_epoch = 0;
			ties_per_epoch = 0;
		}
	}

	//
	// todo: dump the state table in the same format as in the comment header.
	//

	return 0;
}
